package com.jam.commandler.Commandler;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import java.util.function.Consumer;

import org.junit.Test;
import org.mockito.InOrder;

import com.jam.commandler.Argument.ArgumentSet;
import com.jam.commandler.Argument.IntegerArg;
import com.jam.commandler.Argument.StringArg;
import com.jam.commandler.Argument.StringListArg;

public class TestOverloadedArguments extends CommandlerTestBase {

	@Override
	public void setup() {
		super.setup();

		when(sender.hasPermission(anyString())).thenReturn(true);
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void testArgumentSet(){
		Consumer<Object> verifier = (Consumer<Object>) mock(Consumer.class);
		
		cmdlr.getCommandBuilder("fltr")
				.setBehavior(session -> {
					verifier.accept(session.getProcessedArgs().size());
					
					verifier.accept(session.getProcessed("a1"));
					verifier.accept(session.getProcessed("a2"));
					verifier.accept(session.getProcessed("a3"));
				})
				.addArgumentSet(new ArgumentSet(
						new IntegerArg("a1"),
						new StringArg("a2"),
						new IntegerArg("a3")))
				.addArgumentSet(new ArgumentSet(
						new StringArg("a1"),
						new StringListArg("a2")))
				.addArgumentSet(new ArgumentSet());
		
		cmdlr.build();
		
		runCommand("fltr");
		runCommand("fltr word,word,2");
		runCommand("fltr 2 hey -20");
		runCommand("fltr 2 hey -20 100dasda");
		
		InOrder io = inOrder(verifier);
		InOrder io2 = inOrder(sender);
		
		io.verify(verifier).accept(0);
		io.verify(verifier, times(3)).accept(null);
		
		io2.verify(sender, times(1)).sendMessage("§cNo matching command variant found");
		io2.verify(sender, times(1)).sendMessage("§afltr§6");
		io2.verify(sender, times(1)).sendMessage("§afltr§6 <a1> <a2>");
		io2.verify(sender, times(1)).sendMessage("§afltr§6 <a1> <a2> <a3>");
		
		io2.verify(sender, times(1)).sendMessage("§cNo matching command variant found");
		io2.verify(sender, times(1)).sendMessage("§afltr§6");
		io2.verify(sender, times(1)).sendMessage("§afltr§6 <a1> <a2>");
		io2.verify(sender, times(1)).sendMessage("§afltr§6 <a1> <a2> <a3>");
		
		io.verify(verifier).accept(3);
		io.verify(verifier).accept(2);
		io.verify(verifier).accept("hey");
		io.verify(verifier).accept(-20);
	}
}
