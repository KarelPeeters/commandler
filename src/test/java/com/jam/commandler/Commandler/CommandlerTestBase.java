/**
This file is part of Commandler.

Commandler is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Commandler is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Commandler.  If not, see <http://www.gnu.org/licenses/>.
*/

package com.jam.commandler.Commandler;

import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.plugin.java.JavaPlugin;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.jam.commandler.Argument.CommandSession;

@RunWith(PowerMockRunner.class)
@PrepareForTest({JavaPlugin.class, PluginCommand.class})
public abstract class CommandlerTestBase {
	protected Commandler cmdlr;
	
	protected Behavior someBehavior;
	protected Behavior neverExecuteBehavior;
	protected Behavior alwaysExecuteBehavior;
	
	protected Checkpoint c;
	
	protected CommandSender sender;
	
	protected JavaPlugin plugin;
	
	protected interface Checkpoint {
		void check();
	}
	
	private class CmdArgsPair{
		public String name;
		public String[] args;
	}
	
	@Before
	public void setup(){
		someBehavior = (session) -> {};
		neverExecuteBehavior = (session) -> {
			fail("This behavior should never execute!");
		};
		alwaysExecuteBehavior = spy(new Behavior() {
			@Override
			public void execute(CommandSession session) {}
		});
		c = mock(Checkpoint.class);
		sender = mock(CommandSender.class);
		
		//SUCK IT BUKKIT
		plugin = PowerMockito.mock(JavaPlugin.class);
		PluginCommand mockCmd = PowerMockito.mock(PluginCommand.class);

		PowerMockito.when(plugin.getName()).thenReturn("PluginName");
		PowerMockito.when(plugin.getCommand(anyString())).thenReturn(mockCmd);

		cmdlr = new Commandler(plugin);
	}
	
	protected void verifyAlwaysExecutedBehavior(){
		verify(alwaysExecuteBehavior, atLeastOnce()).execute(any(CommandSession.class));
	}
	
	private CmdArgsPair extractCmdAndArgs(String commandString, boolean checkLastSpace){
		CmdArgsPair pair = new CmdArgsPair();
		
		String[] split = commandString.split(" ");
		pair.name = split[0];
		boolean endsWithSpace = checkLastSpace ? commandString.endsWith(" ") : false;
		pair.args = new String[split.length - (endsWithSpace ? 0 : 1)];
		
		if (split.length > 1)
			System.arraycopy(split, 1, pair.args, 0, split.length - 1);
		
		if (endsWithSpace)
			pair.args[pair.args.length - 1] = "";
		
		return pair;
	}
	
	protected void runCommand(String commandString){
		if (commandString.isEmpty()) return;
		
		CmdArgsPair pair = extractCmdAndArgs(commandString, false);
		
		Command cmd = mock(Command.class);
		when(cmd.getName()).thenReturn(pair.name);
		
		cmdlr.onCommand(sender, cmd, null, pair.args);
	}
	
	protected List<String> tabCompleteCommand(String commandString){
		if (commandString.isEmpty()) return null;
		
		CmdArgsPair pair = extractCmdAndArgs(commandString, true);
		
		Command cmd = mock(Command.class);
		when(cmd.getName()).thenReturn(pair.name);
		
		return cmdlr.tabCompleter.onTabComplete(sender, cmd, null, pair.args);
	}
}
