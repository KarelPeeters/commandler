/**
This file is part of Commandler.

Commandler is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Commandler is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Commandler.  If not, see <http://www.gnu.org/licenses/>.
*/

package com.jam.commandler.Commandler;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.bukkit.permissions.PermissionAttachmentInfo;
import org.junit.Test;
import org.mockito.Mockito;

import com.jam.commandler.Argument.CommandSession;

public class TestPermissions extends CommandlerTestBase{
	protected TypeBuilderStep fac;
	
	protected TypeBuilderStep facWord;
	protected TypeBuilderStep facGroup;
	
	protected TypeBuilderStep facWordAdd;
	protected TypeBuilderStep facGroupAdd;
	
	@Override
	public void setup() {
		super.setup();
		
		cmdlr = new Commandler(plugin);
		
		fac = cmdlr.getCommandBuilder("fltr");
		
		facWord = fac.addSubcommand("word");
		
		facWordAdd = facWord.addSubcommand("add");
	}
	
	@Test
	public void testTabCompleteNoPerm(){
		when(sender.hasPermission(Mockito.anyString())).thenReturn(false);
		
		facWordAdd
				.setBehavior(neverExecuteBehavior);
		
		cmdlr.build();
		
		List<String> completions = tabCompleteCommand("fltr word add");

		assertTrue(completions.isEmpty());
	}

	@Test
	public void testNoPerm(){
		when(sender.hasPermission(Mockito.anyString())).thenReturn(false);
		
		facWordAdd
				.setBehavior(neverExecuteBehavior);
		
		cmdlr.build();
		
		runCommand("fltr word add");
		
		verify(sender).sendMessage("§cYou do not have permission to use this command!");
	}
	
	@SuppressWarnings("serial")
	@Test
	public void testTabComplete1Unpermitted(){
		Set<PermissionAttachmentInfo> permSet = new HashSet<PermissionAttachmentInfo>(){{
			PermissionAttachmentInfo ai1 = mock(PermissionAttachmentInfo.class);
			when(ai1.getPermission()).thenReturn("PluginName.fltr");
			PermissionAttachmentInfo ai2 = mock(PermissionAttachmentInfo.class);
			when(ai2.getPermission()).thenReturn("PluginName.fltr.word");
			PermissionAttachmentInfo ai3 = mock(PermissionAttachmentInfo.class);
			when(ai3.getPermission()).thenReturn("PluginName.fltr.word.add");
			
			add(ai1);
			add(ai2);
			add(ai3);
		}};
		
		when(sender.hasPermission("PluginName.fltr.word.add")).thenReturn(true);
		when(sender.hasPermission("PluginName.fltr.group.add")).thenReturn(false);
		when(sender.getEffectivePermissions()).thenReturn(permSet);
		
		facWordAdd.setBehavior(neverExecuteBehavior);
		fac.addSubcommand("group")
			.addSubcommand("add")
			.setBehavior(neverExecuteBehavior);
		
		cmdlr.build();
		
		List<String> completions = tabCompleteCommand("fltr ");
		
		assertFalse(completions.isEmpty());
		assertTrue(completions.contains("word"));
		assertFalse(completions.contains("group"));
		assertEquals(completions.size(), 1);
	}
	
	@Test
	public void testWithPerm(){
		when(sender.hasPermission(Mockito.anyString())).thenReturn(true);
		
		facWordAdd
				.setBehavior(alwaysExecuteBehavior);
		
		cmdlr.build();
		
		runCommand("fltr word add");
		
		verifyAlwaysExecutedBehavior();
	}
	
	@SuppressWarnings("serial")
	@Test
	public void testExistingUnpermitted1(){
		fac.addSubcommand("group")
				.addSubcommand("add")
				.setBehavior(neverExecuteBehavior);
		
		Set<PermissionAttachmentInfo> permSet = new HashSet<PermissionAttachmentInfo>(){{
			PermissionAttachmentInfo ai1 = mock(PermissionAttachmentInfo.class);
			when(ai1.getPermission()).thenReturn("PluginName.fltr");
			PermissionAttachmentInfo ai2 = mock(PermissionAttachmentInfo.class);
			when(ai2.getPermission()).thenReturn("PluginName.fltr.word");
			PermissionAttachmentInfo ai3 = mock(PermissionAttachmentInfo.class);
			when(ai3.getPermission()).thenReturn("PluginName.fltr.word.add");
			
			add(ai1);
			add(ai2);
			add(ai3);
		}};
		
		when(sender.hasPermission("PluginName.fltr.word.add")).thenReturn(true);
		when(sender.hasPermission("PluginName.fltr.group.add")).thenReturn(false);
		when(sender.getEffectivePermissions()).thenReturn(permSet);
		
		facWordAdd
				.setBehavior(neverExecuteBehavior);
		
		cmdlr.build();
		
		runCommand("fltr plumbus");
		runCommand("fltr group");
		
		verify(sender).sendMessage("§cInvalid subcommand \"plumbus\"!");
		verify(sender).sendMessage("§afltr§6 <word>");
		verify(sender).sendMessage("§cYou do not have permission to use this command!");
	}
	
	
	@SuppressWarnings("serial")
	@Test
	public void testOnlySubPerm(){
		fac.addSubcommand("group")
				.addSubcommand("add")
				.setBehavior(neverExecuteBehavior);
		
		Set<PermissionAttachmentInfo> permSet = new HashSet<PermissionAttachmentInfo>(){{
			PermissionAttachmentInfo ai1 = mock(PermissionAttachmentInfo.class);
			when(ai1.getPermission()).thenReturn("PluginName.fltr");
			
			add(ai1);
		}};
		
		when(sender.hasPermission("PluginName.fltr.word.add")).thenReturn(false);
		when(sender.hasPermission("PluginName.fltr.group.add")).thenReturn(false);
		when(sender.getEffectivePermissions()).thenReturn(permSet);
		
		facWordAdd
				.setBehavior(neverExecuteBehavior);
		
		cmdlr.build();
		
		runCommand("fltr group");
		runCommand("fltr");
		
		verify(sender).sendMessage("§cYou do not have permission to use this command!");
		verify(sender).sendMessage("§cMissing subcommand!");
		verify(sender).sendMessage("§afltr§c -");
	}
	
	@SuppressWarnings("serial")
	@Test
	public void testHasPermsAllCommands(){
		fac.addSubcommand("group")
				.addSubcommand("add")
				.setBehavior(alwaysExecuteBehavior);
		
		Set<PermissionAttachmentInfo> permSet = new HashSet<PermissionAttachmentInfo>(){{
			PermissionAttachmentInfo ai1 = mock(PermissionAttachmentInfo.class);
			when(ai1.getPermission()).thenReturn("PluginName.fltr");
			PermissionAttachmentInfo ai2 = mock(PermissionAttachmentInfo.class);
			when(ai2.getPermission()).thenReturn("PluginName.fltr.word");
			PermissionAttachmentInfo ai3 = mock(PermissionAttachmentInfo.class);
			when(ai3.getPermission()).thenReturn("PluginName.fltr.group");
			PermissionAttachmentInfo ai4 = mock(PermissionAttachmentInfo.class);
			when(ai4.getPermission()).thenReturn("PluginName.fltr.word.add");
			PermissionAttachmentInfo ai5 = mock(PermissionAttachmentInfo.class);
			when(ai5.getPermission()).thenReturn("PluginName.fltr.group.add");
			
			add(ai1);
			add(ai2);
			add(ai3);
			add(ai4);
			add(ai5);
		}};
		
		when(sender.hasPermission("PluginName.fltr.word.add")).thenReturn(true);
		when(sender.hasPermission("PluginName.fltr.group.add")).thenReturn(true);
		when(sender.getEffectivePermissions()).thenReturn(permSet);
		
		facWordAdd
				.setBehavior(alwaysExecuteBehavior);
		
		cmdlr.build();
		
		runCommand("fltr cromulon");
		runCommand("fltr word add");
		runCommand("fltr group add");
		
		verify(sender).sendMessage("§cInvalid subcommand \"cromulon\"!");
		verify(sender).sendMessage("§afltr§6 <word|group>");
		
		verify(alwaysExecuteBehavior, times(2)).execute(any(CommandSession.class));
	}
}
