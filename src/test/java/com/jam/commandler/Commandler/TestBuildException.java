/**
This file is part of Commandler.

Commandler is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Commandler is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Commandler.  If not, see <http://www.gnu.org/licenses/>.
*/

package com.jam.commandler.Commandler;

import org.junit.Test;

public class TestBuildException extends CommandlerTestBase {

	@Test(expected = IllegalStateException.class)
	public void testUselessCommand() {
		cmdlr.getCommandBuilder("fltr");
		
		cmdlr.build();
	}
	
	@Test(expected = IllegalStateException.class)
	public void testNestedUseless(){
		cmdlr.getCommandBuilder("fltr")
				.addSubcommand("word");
		
		cmdlr.build();
	}

	@Test(expected = IllegalStateException.class)
	public void testAddSubToGoalCommand() {
		TypeBuilderStep fac = cmdlr.getCommandBuilder("fltr");
		
		fac.setBehavior(someBehavior);
		fac.addSubcommand("test");
		
		cmdlr.build();
	}

	@Test(expected = IllegalStateException.class)
	public void testAddBehaviorToBaseCommand(){
		TypeBuilderStep fac = cmdlr.getCommandBuilder("fltr");
		
		fac.addSubcommand("test");
		fac.setBehavior(someBehavior);
		
		cmdlr.build();
	}
	
	@Test(expected = IllegalStateException.class)
	public void testBuildTwice(){
		cmdlr.getCommandBuilder("fltr")
				.setBehavior(someBehavior);
		
		cmdlr.build();
		cmdlr.build();
	}
}
