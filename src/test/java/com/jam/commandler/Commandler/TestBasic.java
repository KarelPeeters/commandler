/**
This file is part of Commandler.

Commandler is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Commandler is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Commandler.  If not, see <http://www.gnu.org/licenses/>.
*/

package com.jam.commandler.Commandler;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.function.Consumer;

import org.junit.Test;
import org.mockito.InOrder;

import com.jam.commandler.Argument.Arg;
import com.jam.commandler.Argument.CommandSession;
import com.jam.commandler.Argument.ExcessArg;
import com.jam.commandler.Argument.IntegerArg;
import com.jam.commandler.Argument.MergeRemainingArg;
import com.jam.commandler.Argument.MultiStringArg;
import com.jam.commandler.Argument.ProcessedArguments;
import com.jam.commandler.Argument.StringArg;
import com.jam.commandler.Argument.StringListArg;
import com.jam.commandler.ErrorHandler.DefaultArgumentErrorHandler;
import com.jam.commandler.Exception.BadArgumentException;
import com.jam.commandler.Exception.CommandArgumentException;
import com.jam.commandler.Exception.CommandException;
import com.jam.commandler.Permission.PermissionHandler;

public class TestBasic extends CommandlerTestBase {
	
	@Override
	public void setup() {
		super.setup();

		when(sender.hasPermission(anyString())).thenReturn(true);
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void testStringListArg(){
		Consumer<String> verifier = (Consumer<String>) mock(Consumer.class);
		
		cmdlr.getCommandBuilder("fltr")
				.setBehavior(session -> {
					((List<String>) session.getProcessed("arg")).forEach(verifier);;
				})
				.addArg(new StringListArg("arg"));
		
		cmdlr.build();

		runCommand("fltr one,two,three");
		runCommand("fltr one");
		runCommand("fltr ,");
		runCommand("fltr one,");
		runCommand("fltr ,one");
		runCommand("fltr ,,");

		InOrder io = inOrder(verifier);
		
		io.verify(verifier).accept("one");
		io.verify(verifier).accept("two");
		io.verify(verifier).accept("three");
		
		io.verify(verifier, times(3)).accept("one");
		
		io.verify(verifier, never()).accept(anyString());
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void testStringListArgCustomSeparator(){
		Consumer<String> verifier = (Consumer<String>) mock(Consumer.class);
		
		cmdlr.getCommandBuilder("fltr")
				.setBehavior(session -> {
					((List<String>) session.getProcessed("arg")).forEach(verifier);;
				})
				.addArg(new StringListArg("arg", "\\|\\|"));
		
		cmdlr.build();

		runCommand("fltr one||two||three");
		runCommand("fltr one");
		runCommand("fltr ||");
		runCommand("fltr one||");
		runCommand("fltr ||one");
		runCommand("fltr ||||");

		InOrder io = inOrder(verifier);
		
		io.verify(verifier).accept("one");
		io.verify(verifier).accept("two");
		io.verify(verifier).accept("three");
		
		io.verify(verifier, times(3)).accept("one");
		
		io.verify(verifier, never()).accept(anyString());
	}
	
	@Test
	public void testAliases(){
		Behavior forAllAliases = spy(new Behavior() {
			@Override
			public void execute(CommandSession session) {}
		});
		
		cmdlr.getCommandBuilder("fltr")
				.addAliases("filter", "f")
				.setBehavior(forAllAliases);
		
		cmdlr.build();
		
		String[] names = {"fltr", "f", "filter", "garbage", "filler"};
		
		for (String name : names){
			runCommand(name);	
		}
		
		verify(forAllAliases, times(3)).execute(any(CommandSession.class));
	}
	
	@Test
	public void eclemmaStaticClassWorkaround(){
		new PermissionHandler();
	}
	
	@Test
	public void testCommandWithSimpleArg(){
		cmdlr.getCommandBuilder("fltr")
				.setBehavior((session) -> {
					c.check();
					
					ProcessedArguments args = session.getProcessedArgs();
					
					assertEquals(args.size(), 1);
					
					assertEquals((String) args.getProcessed("argument"), "testArg123");
					
					assertNull(args.getProcessed("random"));
				})
				.addArg(new StringArg("argument"));
		
		cmdlr.build();
		
		runCommand("fltr testArg123");
		
		verify(c).check();
	}
	
	@Test
	public void testIntArgNaN(){
		cmdlr.getCommandBuilder("fltr")
				.setBehavior(neverExecuteBehavior)
				.addArg(new IntegerArg("int"));
		
		cmdlr.build();

		runCommand("fltr a1");
		
		verify(sender).sendMessage("§ca1 is not a number!");
		verify(sender).sendMessage("§afltr §c<int>§6");
	}
	
	@Test
	public void testIntArgBadRange(){
		cmdlr.getCommandBuilder("fltr")
				.setBehavior(neverExecuteBehavior)
				.addArg(new IntegerArg("int", 0, 50));
		
		cmdlr.build();

		runCommand("fltr -1");
		runCommand("fltr 100");
		runCommand("fltr 50");
		
		verify(sender, times(3)).sendMessage("§afltr §c<int>§6");
		verify(sender).sendMessage("§c-1 is not within bounds! Try within [0, 50)");
		verify(sender).sendMessage("§c100 is not within bounds! Try within [0, 50)");
		verify(sender).sendMessage("§c50 is not within bounds! Try within [0, 50)");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void testIntArg1(){
		Consumer<Integer> verifier = (Consumer<Integer>) mock(Consumer.class);
		
		cmdlr.getCommandBuilder("fltr")
				.setBehavior(session -> {
					verifier.accept((int) session.getProcessedArgs().getProcessed("int"));
				})
				.addArg(new IntegerArg("int"));
		
		cmdlr.build();

		runCommand("fltr 0");
		runCommand("fltr -2147483648");
		runCommand("fltr 2147483647");
		runCommand("fltr 100000");
		runCommand("fltr 00005");
		runCommand("fltr -0");

		InOrder io = inOrder(verifier);
		
		io.verify(verifier).accept(0);
		io.verify(verifier).accept(-2147483648);
		io.verify(verifier).accept(2147483647);
		io.verify(verifier).accept(100000);
		io.verify(verifier).accept(5);
		io.verify(verifier).accept(0);
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void testIntArg2(){
		Consumer<Integer> verifier = (Consumer<Integer>) mock(Consumer.class);
		
		cmdlr.getCommandBuilder("fltr")
				.setBehavior(session -> {
					verifier.accept((int) session.getProcessedArgs().getProcessed("int"));
				})
				.addArg(new IntegerArg("int", -10, 20));
		
		cmdlr.build();

		runCommand("fltr 0");
		runCommand("fltr -10");
		runCommand("fltr 19");
		runCommand("fltr 11");

		InOrder io = inOrder(verifier);
		
		io.verify(verifier).accept(0);
		io.verify(verifier).accept(-10);
		io.verify(verifier).accept(19);
		io.verify(verifier).accept(11);
	}
	
	@Test
	public void testWithAndWithoutArgInSequence(){
		cmdlr.getCommandBuilder("fltr")
				.setBehavior(alwaysExecuteBehavior);
		
		cmdlr.build();
		
		runCommand("fltr someArg");
		
		verifyAlwaysExecutedBehavior();
		verify(sender, never()).sendMessage(anyString());
		
		runCommand("fltr");
		
		verifyAlwaysExecutedBehavior();
		verify(sender, never()).sendMessage(anyString());
	}
	
	@Test
	public void testCommandWithMultipleArgs(){
		cmdlr.getCommandBuilder("fltr")
				.setBehavior((session) -> {
					c.check();
					
					ProcessedArguments args = session.getProcessedArgs();
					
					assertEquals(args.size(), 3);
					
					assertEquals((String) args.getProcessed("argument1"), "arg1");
					assertEquals((String) args.getProcessed("argument2"), "arg2");
					assertEquals((String) args.getProcessed("argument3"), "arg3");
					
					assertNull(args.getProcessed("random"));
				})
				.addArg(new StringArg("argument1"))
				.addArg(new StringArg("argument2"))
				.addArg(new StringArg("argument3"));
		
		cmdlr.build();
		
		runCommand("fltr arg1 arg2 arg3");
		
		verify(c).check();
	}
	
	@Test
	public void testCommandWithExcessArgs(){
		cmdlr.getCommandBuilder("fltr")
				.setBehavior((session) -> {
					c.check();
					
					ProcessedArguments args = session.getProcessedArgs();
					
					assertEquals(args.size(), 3);
					
					assertEquals((String) args.getProcessed("argument1"), "arg1");
					assertEquals((String) args.getProcessed(ExcessArg.getExcessArgName(0)), "e1");
					assertEquals((String) args.getProcessed(ExcessArg.getExcessArgName(1)), "e2");
					
					assertNull(args.getProcessed("random"));
				})
				.addArg(new StringArg("argument1"));
		
		cmdlr.build();
		
		runCommand("fltr arg1 e1 e2");
		
		verify(c).check();
	}
	
	@Test
	public void testCommandWithCustomProcessor(){
		cmdlr.getCommandBuilder("fltr")
				.setBehavior((session) -> {
					c.check();
					
					ProcessedArguments args = session.getProcessedArgs();
					
					assertEquals(args.size(), 2);
					
					assertEquals((String) args.getProcessed("original"), "hello cruel world");
					assertEquals((String) args.getProcessed("replacement"), "hello forgiving universe");
					
					assertNull(args.getProcessed("random"));
				})
				.addArg(new MultiStringArg("original"))
				.addArg(new MultiStringArg("replacement"));
		
		cmdlr.build();

		runCommand("fltr \"hello cruel world\" \"hello forgiving universe\"");
		
		verify(c).check();
	}
	
	@Test
	public void testMissingDelimiterMultiStringArg(){
		cmdlr.getCommandBuilder("fltr")
				.setBehavior((session) -> {					
					ProcessedArguments args = session.getProcessedArgs();
					
					assertEquals(args.size(), 2);
					
					assertEquals((String) args.getProcessed("original"), "hello cruel world");
					assertEquals((String) args.getProcessed("replacement"), "hello forgiving universe");
					
					assertNull(args.getProcessed("random"));
				})
				.addArg(new MultiStringArg("original"))
				.addArg(new MultiStringArg("replacement"));

		cmdlr.build();
		
		runCommand("fltr \"hello cruel world\" \"hello forgiving universe");
		
		verify(sender).sendMessage("§cMissing argument delimiter \" !");
		verify(sender).sendMessage("§afltr <original> §c<replacement>§6");
	}
	
	@Test
	public void testCustomArgBadArg(){
		cmdlr.getCommandBuilder("fltr")
				.setBehavior(neverExecuteBehavior)
				.addArg(new Arg<String>("arg"){
					@Override
					public String process(CommandSession session) {
						session.getRawArgIterator().next();
						
						throw new BadArgumentException();
					}
				});
		
		cmdlr.build();

		runCommand("fltr word");
		
		verify(sender).sendMessage("§cInvalid argument \"word\"!");
		verify(sender).sendMessage("§afltr §c<arg>§6");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void testMultiStringArg(){
		Consumer<String> verifier = (Consumer<String>) mock(Consumer.class);
		
		cmdlr.getCommandBuilder("fltr")
				.setBehavior((session) -> {					
					ProcessedArguments args = session.getProcessedArgs();
					
					verifier.accept((String) args.getProcessed("arg"));
				})
				.addArg(new MultiStringArg("arg"));
		
		cmdlr.build();

		runCommand("fltr word");
		runCommand("fltr \"word\"");
		runCommand("fltr \" word\"");
		runCommand("fltr \" word \"");
		runCommand("fltr \"word \"");
		runCommand("fltr \"word test\"");
		runCommand("fltr \" word test\"");
		runCommand("fltr \" word test \"");
		runCommand("fltr \"word test \"");
		
		InOrder io = inOrder(verifier);
		
		io.verify(verifier, times(2)).accept("word");
		io.verify(verifier).accept(" word");
		io.verify(verifier).accept(" word ");
		io.verify(verifier).accept("word ");
		io.verify(verifier).accept("word test");
		io.verify(verifier).accept(" word test");
		io.verify(verifier).accept(" word test ");
		io.verify(verifier).accept("word test ");
	}
	
	@Test
	public void testUnrecognizedArgException(){
		cmdlr.getCommandBuilder("fltr")
				.setBehavior(neverExecuteBehavior)
				.addArg(new StringArg("test"){
					@Override
					public String process(CommandSession session) {
						session.getRawArgIterator().next();
						
						throw new CommandArgumentException();
					}
				});
		
		cmdlr.build();

		runCommand("fltr test");
		
		verify(sender).sendMessage("§cArgument error in \"test\"!");
	}
	
	@Test
	public void testDefaultNoArgumentErrorHandler(){
		DefaultArgumentErrorHandler handler = spy(DefaultArgumentErrorHandler.class);
		
		cmdlr.getCommandBuilder("fltr")
				.setBehavior(neverExecuteBehavior)
				.setArgErrorHandler(handler)
				.addArg(new StringArg("argument"));
		
		cmdlr.build();

		runCommand("fltr");
		
		verify(sender).sendMessage("§cMissing command argument!");
		verify(sender).sendMessage("§afltr §c<argument>§6");
	}
	
	@Test
	public void testCommandlerFromBuilder(){
		cmdlr.buildFromBuilder(new CommandlerBuilder() {
			@Override
			public void build(Commandler commandler) {
				cmdlr.getCommandBuilder("fltr")
						.setBehavior(alwaysExecuteBehavior);
			}
		});
		
		runCommand("fltr");
		
		verifyAlwaysExecutedBehavior();
	}
	
	@Test
	public void testCommandEquals(){
		BaseCommand cmd = new BaseCommand("whatever");
		BaseCommand cmd2 = new BaseCommand("whatever");
		Integer num = 5;
		
		assertTrue(cmd.equals(cmd2));
		assertFalse(cmd.equals(num));
	}
	
	@Test
	public void testArgEquals(){
		StringArg arg1 = new StringArg("whatever");
		StringArg arg2 = new StringArg("whatever");
		Integer num = 5;
		
		assertTrue(arg1.equals(arg2));
		assertFalse(arg1.equals(num));
	}
	
	@Test
	public void testMergeRemainingArgs(){
		cmdlr.getCommandBuilder("fltr")
				.setBehavior((session) -> {
					c.check();
					
					ProcessedArguments args = session.getProcessedArgs();
					
					assertEquals(args.size(), 1);
					
					assertEquals((String) args.getProcessed("arg"), "today on how they do it- plumbuses!");
					
					assertNull(args.getProcessed("random"));
				})
				.addArg(new MergeRemainingArg("arg"));
		
		cmdlr.build();

		runCommand("fltr today on how they do it- plumbuses!");

		verify(c).check();
	}
	
	@Test
	public void testOptionalArg1(){
		cmdlr.getCommandBuilder("fltr")
				.setBehavior(alwaysExecuteBehavior)
				.addArg(new Arg<String>("arg", true) {
					@Override
					public String process(CommandSession session) {
						fail("Should never get here");
						return null;
					}
				});
		
		cmdlr.build();

		runCommand("fltr");
		
		verifyAlwaysExecutedBehavior();
	}
	
	@Test
	public void testOptionalArg2(){
		cmdlr.getCommandBuilder("fltr")
				.setBehavior((session) -> {
					c.check();
					
					ProcessedArguments args = session.getProcessedArgs();
					
					assertEquals(args.size(), 1);
					
					assertEquals((String) args.getProcessed("arg"), "test1");
					
					assertNull(args.getProcessed("random"));
				})
				.addArg(new Arg<String>("arg", true) {
					@Override
					public String process(CommandSession session) {
						return session.getRawArgIterator().next();
					}
				});
		
		cmdlr.build();

		runCommand("fltr test1");
		
		verify(c).check();
	}
	
	@SuppressWarnings("serial")
	@Test
	public void testUnhandledError(){
		cmdlr.getCommandBuilder("fltr")
				.setBehavior((session) -> {
					c.check();
					
					throw new CommandException(null){};
				});
		
		cmdlr.build();

		runCommand("fltr");
		
		verify(c).check();
		verify(sender).sendMessage("Unhandled plugin error");;
	}
	
	@Test
	public void testOptionalSignature(){
		Arg<String> a = new Arg<String>("name", true) {
			@Override
			public String process(CommandSession session) {
				return null;
			}
		};
		
		assertEquals("[name]", a.getSignature());
	}
	
	@Test(expected = IllegalAccessError.class)
	public void testPeekWithoutCurrent(){
		cmdlr.getCommandBuilder("fltr")
				.setBehavior(neverExecuteBehavior)
				.addArg(new StringArg("argument"){
					@Override
					public String process(CommandSession session) {
						session.getRawArgIterator().peek();
						
						return super.process(session);
					}
				});
		
		cmdlr.build();

		runCommand("fltr testArg123");
	}
}
