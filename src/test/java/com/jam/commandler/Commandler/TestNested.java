/**
This file is part of Commandler.

Commandler is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Commandler is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Commandler.  If not, see <http://www.gnu.org/licenses/>.
*/

package com.jam.commandler.Commandler;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.bukkit.Server;
import org.bukkit.command.BlockCommandSender;
import org.bukkit.entity.Player;
import org.junit.Test;

import com.jam.commandler.Argument.ExcessArg;
import com.jam.commandler.Argument.ProcessedArguments;
import com.jam.commandler.Argument.StringArg;

public class TestNested extends CommandlerTestBase{
	protected TypeBuilderStep fac;
	
	protected TypeBuilderStep facWord;
	protected TypeBuilderStep facGroup;
	
	protected TypeBuilderStep facWordAdd;
	protected TypeBuilderStep facGroupAdd;
	
	@Override
	public void setup() {
		super.setup();
		
		when(sender.hasPermission(anyString())).thenReturn(true);
		
		fac = cmdlr.getCommandBuilder("fltr");
		
		facWord = fac.addSubcommand("word");
		
		facWordAdd = facWord.addSubcommand("add");
	}
	
	@Test
	public void testTabCompleteNoNext(){
		facWordAdd.setBehavior(neverExecuteBehavior);

		cmdlr.build();
		
		List<String> completions = tabCompleteCommand("fltr");
		
		assertTrue(completions.isEmpty());
	}
	
	@Test
	public void testTabCompleteSub1(){
		facWordAdd.setBehavior(neverExecuteBehavior);

		cmdlr.build();
		
		List<String> completions = tabCompleteCommand("fltr ");
		
		assertFalse(completions.isEmpty());
		assertTrue(completions.contains("word"));
		assertEquals(completions.size(), 1);
	}
	
	@Test
	public void testTabCompleteSub2(){
		facWordAdd.setBehavior(neverExecuteBehavior);
		fac.addSubcommand("group")
			.addSubcommand("add")
			.setBehavior(neverExecuteBehavior);
		
		cmdlr.build();
		
		List<String> completions = tabCompleteCommand("fltr ");
		
		assertFalse(completions.isEmpty());
		assertTrue(completions.contains("word"));
		assertTrue(completions.contains("group"));
		assertEquals(completions.size(), 2);
	}
	
	@Test
	public void testTabCompleteSub3(){
		facWordAdd.setBehavior(neverExecuteBehavior);
		fac.addSubcommand("group")
			.addSubcommand("add")
			.setBehavior(neverExecuteBehavior);
		
		cmdlr.build();
		
		List<String> completions = tabCompleteCommand("fltr word");
		
		assertTrue(completions.isEmpty());
	}
	
	@Test
	public void testTabCompleteSub4(){
		facWordAdd.setBehavior(neverExecuteBehavior);
		fac.addSubcommand("group")
			.addSubcommand("add")
			.setBehavior(neverExecuteBehavior);
		
		cmdlr.build();
		
		List<String> completions = tabCompleteCommand("fltr word ");
		
		assertFalse(completions.isEmpty());
		assertTrue(completions.contains("add"));
		assertEquals(completions.size(), 1);
	}
	
	@Test
	public void testTabCompleteArg1(){
		facWordAdd.setBehavior(neverExecuteBehavior);
		cmdlr.build();
		
		List<String> completions = tabCompleteCommand("fltr word add");
		
		assertTrue(completions.isEmpty());
	}
	
	@Test
	public void testTabCompleteArg2(){
		Collection<Player> players = new ArrayList<Player>();
		Player p1 = mock(Player.class);
		Player p2 = mock(Player.class);
		Player p3 = mock(Player.class);
		
		when(p1.getName()).thenReturn("JamMasterz");
		when(p2.getName()).thenReturn("HummaKavula");
		when(p3.getName()).thenReturn("Jodelina");
		
		sender = mock(Player.class);
		when(sender.hasPermission(anyString())).thenReturn(true);
		when(((Player) sender).canSee(p1)).thenReturn(true);
		when(((Player) sender).canSee(p2)).thenReturn(true);
		when(((Player) sender).canSee(p3)).thenReturn(false);
		
		players.add(p1);
		players.add(p2);
		players.add(p3);
		
		Server server = mock(Server.class);
		doReturn(players).when(server).getOnlinePlayers();
		when(sender.getServer()).thenReturn(server);
		
		facWordAdd.setBehavior(neverExecuteBehavior);
		cmdlr.build();
		
		List<String> completions = tabCompleteCommand("fltr word add ");
		
		assertFalse(completions.isEmpty());
		assertTrue(completions.contains("JamMasterz"));
		assertTrue(completions.contains("HummaKavula"));
		assertEquals(completions.size(), 2);
		
		sender = mock(BlockCommandSender.class);
		when(sender.hasPermission(anyString())).thenReturn(true);
		when(sender.getServer()).thenReturn(server);
		
		completions = tabCompleteCommand("fltr word add ");
		
		assertFalse(completions.isEmpty());
		assertTrue(completions.contains("JamMasterz"));
		assertTrue(completions.contains("HummaKavula"));
		assertTrue(completions.contains("Jodelina"));
		assertEquals(completions.size(), 3);
	}
	
	@Test
	public void testExecuteRightBehavior(){
		facGroup = fac.addSubcommand("group");
		facGroupAdd = facGroup.addSubcommand("add");
		
		facWordAdd
				.setBehavior(alwaysExecuteBehavior);
		facGroupAdd
				.setBehavior((session) -> {
					fail();
				});
		
		cmdlr.build();
		
		runCommand("fltr word add");
		
		verifyAlwaysExecutedBehavior();
	}
	
	@Test
	public void testNestedAliases(){
		//TODO:
	}
	
	@Test
	public void testNestedCommandWithSimpleArg(){
		facWordAdd
				.setBehavior((session) -> {
					c.check();
					
					ProcessedArguments args = session.getProcessedArgs();
					
					assertEquals(args.size(), 1);
					
					assertEquals((String) args.getProcessed("argument"), "testArg123");
					
					assertNull(args.getProcessed("random"));
				})
				.addArg(new StringArg("argument"));
		
		cmdlr.build();
		
		runCommand("fltr word add testArg123");
		
		verify(c).check();
	}
	
	@Test
	public void testNestedCommandWithMultipleArgs(){
		facWordAdd
				.setBehavior((session) -> {
					c.check();
					
					ProcessedArguments args = session.getProcessedArgs();
					
					assertEquals(args.size(), 3);
					
					assertEquals((String) args.getProcessed("argument1"), "arg1");
					assertEquals((String) args.getProcessed("argument2"), "arg2");
					assertEquals((String) args.getProcessed("argument3"), "arg3");
					
					assertNull(args.getProcessed("random"));
				})
				.addArg(new StringArg("argument1"))
				.addArg(new StringArg("argument2"))
				.addArg(new StringArg("argument3"));
		
		cmdlr.build();
		
		runCommand("fltr word add arg1 arg2 arg3");
		
		verify(c).check();
	}
	
	@Test
	public void testNestedCommandWithExcessArgs(){
		facWordAdd
				.setBehavior((session) -> {
					c.check();
					
					ProcessedArguments args = session.getProcessedArgs();
					
					assertEquals(args.size(), 3);
					
					assertEquals((String) args.getProcessed("argument1"), "arg1");
					assertEquals((String) args.getProcessed(ExcessArg.getExcessArgName(0)), "e1");
					assertEquals((String) args.getProcessed(ExcessArg.getExcessArgName(1)), "e2");
					
					assertNull(args.getProcessed("random"));
				})
				.addArg(new StringArg("argument1"));
		
		cmdlr.build();
		
		runCommand("fltr word add arg1 e1 e2");
		
		verify(c).check();
	}
	
	@Test
	public void testBadSubCommand1(){
		facWordAdd
				.setBehavior((session) -> {});
		
		cmdlr.build();
		
		runCommand("fltr word bad");
		
		verify(sender).sendMessage("§cInvalid subcommand \"bad\"!");
		verify(sender).sendMessage("§afltr word§6 <add>");
	}
	
	@Test
	public void testBadSubCommand2(){
		facWordAdd.setBehavior((session) -> {});
	
		facGroup = fac.addSubcommand("group");
		facGroup.setBehavior(someBehavior);
		
		cmdlr.build();
		
		runCommand("fltr bad");
		
		verify(sender).sendMessage("§cInvalid subcommand \"bad\"!");
		verify(sender).sendMessage("§afltr§6 <word|group>");
	}
	
	@Test
	public void testBadSubCommand3(){
		facWordAdd.setBehavior((session) -> {});
	
		facGroup = fac.addSubcommand("group");
		facGroup.setBehavior(someBehavior);
		
		cmdlr.build();
		
		runCommand("fltr bad untested irrelevant whatever");
		
		verify(sender).sendMessage("§cInvalid subcommand \"bad\"!");
		verify(sender).sendMessage("§afltr§6 <word|group>");
	}
	
	@Test
	public void testNestedCalled(){
		facWordAdd
				.setBehavior((session) -> {
					c.check();
				});
		
		cmdlr.build();

		runCommand("fltr word add");
		
		verify(c).check();
	}
	
	@Test
	public void testMissingSubCommand(){
		facWordAdd.setBehavior(neverExecuteBehavior);
	
		cmdlr.build();
		
		runCommand("fltr");
		
		verify(sender).sendMessage("§cMissing subcommand!");
		verify(sender).sendMessage("§afltr§6 <word>");
	}
}
