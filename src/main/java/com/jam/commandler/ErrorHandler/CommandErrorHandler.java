/**
This file is part of Commandler.

Commandler is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Commandler is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Commandler.  If not, see <http://www.gnu.org/licenses/>.
*/

package com.jam.commandler.ErrorHandler;

import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import com.jam.commandler.Argument.CommandSession;
import com.jam.commandler.Commandler.BaseCommand;
import com.jam.commandler.Commandler.Command;
import com.jam.commandler.Exception.BadSubcommandException;
import com.jam.commandler.Exception.CommandException;
import com.jam.commandler.Exception.MissingSubCommandException;
import com.jam.commandler.Exception.PermissionException;

public class CommandErrorHandler {
	public static final ChatColor GOOD = ChatColor.GREEN;
	public static final ChatColor BAD = ChatColor.RED;
	public static final ChatColor UNCHECKED = ChatColor.GOLD;
	
	public void handle(CommandSession session, CommandException error){
		CommandSender sender = session.getSender();
		
		if (error instanceof BadSubcommandException){
			sender.sendMessage(BAD + "Invalid subcommand \"" + session.getRawArgIterator().peek() + "\"!");
			sender.sendMessage(getCommandSignature(session, (BaseCommand) error.cmd));
		} else if (error instanceof PermissionException){
			sender.sendMessage(BAD + "You do not have permission to use this command!");
		} else if (error instanceof MissingSubCommandException){
			sender.sendMessage(BAD + "Missing subcommand!");
			sender.sendMessage(getCommandSignature(session, (BaseCommand) error.cmd));
		} else {
			System.out.println("Unhandled CommandException: " + error.getClass().getName());
			sender.sendMessage("Unhandled plugin error");
		}
	}
	
	protected String getCommandSignature(CommandSession session, BaseCommand command){
		StringBuilder msg = new StringBuilder();
		
		List<Command> subCommands = command.getPermittedSubCommandList(session.getSender());
		
		msg.append(GOOD.toString());
		msg.append(session.getBase());
		
		if (subCommands.isEmpty()){
			msg.append(BAD.toString());
			msg.append(" -");
		} else {
			msg.append(UNCHECKED.toString());
			msg.append(" <");
			for (Command c : subCommands){
				msg.append(c.getName());
				msg.append("|");
			}
			msg.deleteCharAt(msg.length() - 1);
			msg.append(">");
		}

		return msg.toString();
	}
}
