/**
This file is part of Commandler.

Commandler is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Commandler is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Commandler.  If not, see <http://www.gnu.org/licenses/>.
*/

package com.jam.commandler.ErrorHandler;

import org.bukkit.ChatColor;

import com.jam.commandler.Argument.CommandSession;
import com.jam.commandler.Argument.ProcessedArguments;
import com.jam.commandler.Exception.CommandArgumentException;

public abstract class ArgumentErrorHandler {
	public static final ChatColor GOOD = ChatColor.GREEN;
	public static final ChatColor BAD = ChatColor.RED;
	public static final ChatColor UNCHECKED = ChatColor.GOLD;
	
	protected static String getSignature(CommandSession session){
		StringBuilder msg = new StringBuilder();
		
		msg.append(GOOD.toString());
		msg.append(session.getBase());
		
		ProcessedArguments processed = session.getProcessedArgs();
		
		processed.argList().stream().forEach(arg -> {
			msg.append(" ");

			if (processed.getProcessed(arg.getName()) == null){
				msg.append(BAD.toString());
				msg.append(arg.getSignature());
				msg.append(UNCHECKED.toString());
			} else {
				msg.append(arg.getSignature());
			}
		});
		
		return msg.toString();
	}
	
	public abstract void handle(CommandSession session, CommandArgumentException error);
}
