/**
This file is part of Commandler.

Commandler is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Commandler is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Commandler.  If not, see <http://www.gnu.org/licenses/>.
*/

package com.jam.commandler.ErrorHandler;

import java.util.LinkedList;
import java.util.List;

import org.bukkit.command.CommandSender;

import com.jam.commandler.Argument.Arg;
import com.jam.commandler.Argument.ArgumentSet;
import com.jam.commandler.Argument.CommandSession;
import com.jam.commandler.Argument.RawArgIterator;
import com.jam.commandler.Exception.BadArgumentException;
import com.jam.commandler.Exception.CommandArgumentException;
import com.jam.commandler.Exception.MissingArgumentDelimiterException;
import com.jam.commandler.Exception.MissingArgumentException;
import com.jam.commandler.Exception.NoArgSetException;
import com.jam.commandler.Exception.NotANumberArgumentException;
import com.jam.commandler.Exception.NumberArgumentNotWithinRangeException;

public class DefaultArgumentErrorHandler extends ArgumentErrorHandler{
	@Override
	public void handle(CommandSession session, CommandArgumentException error) {
		CommandSender sender = session.getSender();
		RawArgIterator iter = session.getRawArgIterator();
		
		if (error instanceof MissingArgumentException){
			sender.sendMessage(BAD + "Missing command argument!");
		} else if (error instanceof BadArgumentException){
			sender.sendMessage(BAD + "Invalid argument \"" + iter.peek() + "\"!");
		} else if (error instanceof MissingArgumentDelimiterException){
			sender.sendMessage(BAD + "Missing argument delimiter \" !");
		} else if (error instanceof NotANumberArgumentException) {
			sender.sendMessage(BAD + iter.peek() + " is not a number!");
		} else if (error instanceof NoArgSetException){
			NoArgSetException e = (NoArgSetException) error;
			
			sender.sendMessage(BAD + "No matching command variant found");
			
			getArgSetSignatures(session, e.argSets).stream()
					.sorted()
					.forEach(sender::sendMessage);
			
			return;
		} else if (error instanceof NumberArgumentNotWithinRangeException){
			NumberArgumentNotWithinRangeException e = (NumberArgumentNotWithinRangeException) error;
			
			sender.sendMessage(BAD + iter.peek() + " is not within bounds! Try within [" + e.min + ", " + e.max +")");
		} else {
			System.out.println("Unhandled ArgumentException: " + error.getClass().getName());
			
			sender.sendMessage(BAD + "Argument error in \"" + session.getRawArgIterator().peek() + "\"!");
		}
		
		sender.sendMessage(getSignature(session));
	}
	
	private static List<String> getArgSetSignatures(CommandSession session, Iterable<ArgumentSet> argSets){
		List<String> sigs = new LinkedList<>();
		
		argSets.forEach(a -> sigs.add(getSignature(session, a)));
		
		return sigs;
	}
	
	@SuppressWarnings("rawtypes")
	private static String getSignature(CommandSession session, Iterable<? extends Arg> args){
		StringBuilder msg = new StringBuilder();
		
		msg.append(GOOD.toString());
		msg.append(session.getBase());
		msg.append(UNCHECKED.toString());
		
		args.forEach(arg -> {
			msg.append(" ");

			msg.append(arg.getSignature());
		});
		
		return msg.toString();
	}
}
