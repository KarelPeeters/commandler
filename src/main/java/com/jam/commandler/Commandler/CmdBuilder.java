/**
This file is part of Commandler.

Commandler is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Commandler is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Commandler.  If not, see <http://www.gnu.org/licenses/>.
*/

package com.jam.commandler.Commandler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import com.jam.commandler.Argument.Arg;
import com.jam.commandler.Argument.ArgumentSet;
import com.jam.commandler.ErrorHandler.ArgumentErrorHandler;

@SuppressWarnings("rawtypes")
public class CmdBuilder implements TypeBuilderStep, BehaviorStep, NormalArgsStep, OverloadedArgsStep{
	protected Commandler com;
	
	protected List<CmdBuilder> subCommandBuilders;
	protected Command cmd;

	protected String basePermission;
	
	protected String commandName;
	protected List<String> commandAliases;
	protected Behavior commandBehavior;
	protected HashMap<String, Arg> commandArgs;
	protected Set<ArgumentSet> commandArgSets;
	protected ArgumentErrorHandler commandErrorHandler;
	
	protected CmdBuilder(Commandler com, String commandName, String basePermission){
		this.com = com;
		this.commandName = commandName;
		this.basePermission = basePermission;
		
		this.commandAliases = new LinkedList<>();
		this.commandArgs = new LinkedHashMap<>();
		this.commandArgSets = new LinkedHashSet<>();
		this.subCommandBuilders = new LinkedList<>();
	}
	
	protected Command build(){
		if (!subCommandBuilders.isEmpty()){
			return buildBaseCommand();
		} else if (commandBehavior != null){
			if (!commandArgs.isEmpty()){
				return buildNormalGoalCommand();
			} else if (!commandArgSets.isEmpty()) {
				return buildOverloadedGoalCommand();
			} else {
				commandArgs = new LinkedHashMap<>();
				
				return buildNormalGoalCommand();
			}
		} else {
			throw new IllegalStateException("The command " + commandName + " must either be base or have a behavior!");
		}
	}
	
	private OverloadedGoalCommand buildOverloadedGoalCommand() {
		OverloadedGoalCommand cmd = new OverloadedGoalCommand(commandName);
		
		cmd.aliases = commandAliases;
		cmd.behavior = commandBehavior;
		cmd.errorHandler = commandErrorHandler;
		cmd.argSets = commandArgSets;
		cmd.permission = basePermission + '.' + commandName;
		
		return cmd;
	}

	private NormalGoalCommand buildNormalGoalCommand(){
		NormalGoalCommand cmd = new NormalGoalCommand(commandName);
		
		cmd.aliases = commandAliases;
		cmd.behavior = commandBehavior;
		cmd.errorHandler = commandErrorHandler;
		cmd.args = commandArgs;
		cmd.permission = basePermission + '.' + commandName;
		
		return cmd;
	}
	
	private BaseCommand buildBaseCommand(){
		BaseCommand cmd = new BaseCommand(commandName);
		cmd.aliases = commandAliases;
		
		List<Command> subCommands = new ArrayList<Command>();
		for (CmdBuilder subBuilder : subCommandBuilders){
			subCommands.add(subBuilder.build());
		}
		cmd.permission = basePermission + '.' + commandName;
		
		cmd.subCommands = subCommands;
		
		return cmd;
	}
	
	public static TypeBuilderStep getBuilder(Commandler com, String commandName, String basePermission){
		return new CmdBuilder(com, commandName, basePermission);
	}

	@Override
	public OverloadedArgsStep addArgumentSet(ArgumentSet set) {
		if (!commandArgSets.add(set)){
			throw new IllegalArgumentException("ArgumentSets may not consume the same amount of raw arguments");
		}
		
		return this;
	}

	@Override
	public NormalArgsStep addArg(Arg arg) {
		commandArgs.put(arg.getName(), arg);
		
		return this;
	}

	@Override
	public BehaviorWithHandlerStep setArgErrorHandler(ArgumentErrorHandler handler) {
		commandErrorHandler = handler;
		
		return this;
	}

	@Override
	public TypeBuilderStep addAliases(String... aliases) {
		commandAliases.addAll(Arrays.asList(aliases));
		
		return this;
	}

	@Override
	public BehaviorStep setBehavior(Behavior behavior) {
		if (!subCommandBuilders.isEmpty()){
			throw new IllegalStateException("The command " + commandName + " has subcommands and can't have a goal behavior");
		}
		
		commandBehavior = behavior;
		
		return this;
	}

	@Override
	public TypeBuilderStep addSubcommand(String commandName) {
		if (commandBehavior != null){
			throw new IllegalStateException("The command  " + commandName + " has a goal behavior and can't be a base for subcommands");
		}
		
		CmdBuilder step = new CmdBuilder(com, commandName, basePermission + '.' + this.commandName);
		subCommandBuilders.add(step);
		
		this.cmd = new BaseCommand(commandName);
		
		return step;
	}
}
