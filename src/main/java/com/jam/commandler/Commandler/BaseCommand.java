/**
This file is part of Commandler.

Commandler is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Commandler is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Commandler.  If not, see <http://www.gnu.org/licenses/>.
*/

package com.jam.commandler.Commandler;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.bukkit.command.CommandSender;

import com.jam.commandler.Argument.CommandSession;
import com.jam.commandler.Exception.BadSubcommandException;
import com.jam.commandler.Exception.MissingSubCommandException;

public class BaseCommand extends Command {
	protected List<Command> subCommands;

	public BaseCommand(String name) {
		super(name);
	}

	@Override
	public void execute(CommandSession session) {
		if (!session.getRawArgIterator().hasNext()) {
			throw new MissingSubCommandException(this);
		}

		boolean found = findAndMapCommandOrElse(session.getRawArgIterator().next(), c -> {
			session.appendCurrentToBase();

			c.executeCommand(session);

			return true;
		}, () -> false);

		if (!found) throw new BadSubcommandException(this);
	}

	private <T> T findAndMapCommandOrElse(String sub, Function<Command, T> found, Supplier<T> notFound) {
		return subCommands.stream()
			.filter(c -> c.isInstance(sub))
			.findFirst()
			.map(found)
			.orElseGet(notFound);
	}

	public List<Command> getPermittedSubCommandList(CommandSender sender) {
		return getPermittedSubCommandStream(sender)
			.collect(Collectors.toList());
	}
	
	private Stream<Command> getPermittedSubCommandStream(CommandSender sender){
		return subCommands.stream()
			.filter(c -> c.hasPermission(sender));
	}

	@Override
	public List<String> tabComplete(CommandSession session) {
		//If no next, dont complete
		if (!session.getRawArgIterator().hasNext()) {
			return new ArrayList<>();
		}

		//If next available, check if its a valid subCommand and if so, call tabCompleteCommand recursively
		//Otherwise, tabComplete all available commands that start with whatever the user typed.
		String raw = session.getRawArgIterator().next();

		return findAndMapCommandOrElse(raw,
			c -> c.tabCompleteCommand(session),
			() -> getPermittedSubCommandStream(session.getSender())
				.filter(c -> c.getName().startsWith(raw))
				.map(c -> c.getName())
				.collect(Collectors.toList()));
	}
}
