/**
This file is part of Commandler.

Commandler is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Commandler is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Commandler.  If not, see <http://www.gnu.org/licenses/>.
*/

package com.jam.commandler.Commandler;

import java.util.HashMap;

import com.jam.commandler.Argument.Arg;
import com.jam.commandler.Argument.CommandSession;

@SuppressWarnings("rawtypes")
public class NormalGoalCommand extends GoalCommand {
	protected HashMap<String, Arg> args;

	public NormalGoalCommand(String name) {
		super(name);
	}

	@Override
	public void processArgs(CommandSession session) {
		session.processArgs(args.values());
	}
}
