/**
This file is part of Commandler.

Commandler is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Commandler is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Commandler.  If not, see <http://www.gnu.org/licenses/>.
*/

package com.jam.commandler.Commandler;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.jam.commandler.Argument.CommandSession;
import com.jam.commandler.ErrorHandler.ArgumentErrorHandler;
import com.jam.commandler.ErrorHandler.DefaultArgumentErrorHandler;
import com.jam.commandler.Exception.CommandArgumentException;

public abstract class GoalCommand extends Command {
	protected Behavior behavior;
	protected ArgumentErrorHandler errorHandler;

	public GoalCommand(String name) {
		super(name);
	}
	
	@Override
	public void execute(CommandSession session) {
		try {
			processArgs(session);
			
			behavior.execute(session);
		} catch (CommandArgumentException e) {
			if (errorHandler == null) {
				errorHandler = new DefaultArgumentErrorHandler();
			}
			
			errorHandler.handle(session, e);
		}
	}
	
	public abstract void processArgs(CommandSession session);

	@Override
	public List<String> tabComplete(CommandSession session) {
		String lastArg = session.getRawArgIterator().last();
		
		if (lastArg == null)
			return new ArrayList<>();
		
		final String uncompleteName = lastArg.toLowerCase();
		
		CommandSender sender = session.getSender();
		Player sendingPlayer = (sender instanceof Player) ? (Player) sender : null;
		
		return sender.getServer().getOnlinePlayers().stream()
			.filter(p -> sendingPlayer == null || sendingPlayer.canSee(p))
			.map(p -> p.getName())
			.filter(p -> p.toLowerCase().startsWith(uncompleteName))
			.collect(Collectors.toList());
	}
}
