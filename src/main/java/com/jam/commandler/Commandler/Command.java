/**
This file is part of Commandler.

Commandler is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Commandler is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Commandler.  If not, see <http://www.gnu.org/licenses/>.
*/

package com.jam.commandler.Commandler;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.CommandSender;

import com.jam.commandler.Argument.CommandSession;
import com.jam.commandler.Exception.PermissionException;
import com.jam.commandler.Permission.PermissionHandler;

public abstract class Command {
	protected String name;
	protected List<String> aliases;
	protected String permission;
	
	public Command(String name) {
		this.name = name;
		this.aliases = new ArrayList<String>();
	}
	
	public void executeCommand(CommandSession session){
		if (!hasPermission(session.getSender())){
			throw new PermissionException(this);
		}
		
		execute(session);
	}
	
	public List<String> tabCompleteCommand(CommandSession session){
		if (!hasPermission(session.getSender())){
			return new ArrayList<String>();
		}
		
		return tabComplete(session);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Command){
			Command c = (Command) obj;
			if (c.name.equalsIgnoreCase(this.name)){
				return true;
			}
		}
		
		return false;
	}
	
	public String getName(){
		return name;
	}
	
	public boolean hasPermission(CommandSender sender){
		return permission == null || PermissionHandler.hasPermission(sender, permission);
	}
	
	public boolean isInstance(String cmd) {
		return cmd.equalsIgnoreCase(name) || aliases.contains(cmd);
	}
	
	public abstract void execute(CommandSession session);
	
	public abstract List<String> tabComplete(CommandSession session);
}
