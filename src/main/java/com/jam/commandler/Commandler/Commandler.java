/**
This file is part of Commandler.

Commandler is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Commandler is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Commandler.  If not, see <http://www.gnu.org/licenses/>.
*/

package com.jam.commandler.Commandler;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;

import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.plugin.java.JavaPlugin;

import com.jam.commandler.Argument.CommandSession;
import com.jam.commandler.ErrorHandler.CommandErrorHandler;
import com.jam.commandler.Exception.CommandException;

public class Commandler implements CommandExecutor{
	protected List<Command> bases;
	protected JavaPlugin plugin;
	protected TabCompleter tabCompleter;
	
	protected boolean built;
	protected LinkedList<CmdBuilder> baseBuilders;
	
	protected CommandErrorHandler errorHandler;
	
	public Commandler(JavaPlugin plugin) {
		this.bases = new ArrayList<Command>();
		this.baseBuilders = new LinkedList<>();
		this.plugin = plugin;
		
		this.errorHandler = new CommandErrorHandler();
		this.tabCompleter = new TabCompleter() {
			@Override
			public List<String> onTabComplete(CommandSender sender, org.bukkit.command.Command cmd, String lbl, String[] args) {
				return findAndMapCommandOrElse(cmd.getName(), c -> {
					return c.tabCompleteCommand(new CommandSession(sender, cmd.getName(), args));
				}, () -> new ArrayList<>());
			}
		};
	}
	
	@Override
	public boolean onCommand(CommandSender sender, org.bukkit.command.Command cmd, String label, String[] args) {
		return findAndMapCommandOrElse(cmd.getName(), c -> {
			CommandSession session = new CommandSession(sender, cmd.getName(), args);
			
			try {
				c.executeCommand(session);
			} catch (CommandException e){
				errorHandler.handle(session, e);
			}
			
			return true;
		}, () -> false);
	}
	
	protected <T> T findAndMapCommandOrElse(String cmd, Function<Command, T> func, Supplier<T> notFound){
		return bases.stream()
			.filter(c -> c.isInstance(cmd))
			.findFirst()
			.map(func)
			.orElseGet(notFound);
	}
	
	public void buildFromBuilder(CommandlerBuilder builder){
		builder.build(this);
		
		build();
	}
	
	public void build(){
		if (built)
			throw new IllegalStateException("You can't build commands twice");
			
		for (CmdBuilder b : baseBuilders){
			addCommand(b.build());
		}
		
		built = true;
	}
	
	public TypeBuilderStep getCommandBuilder(String command){
		baseBuilders.add((CmdBuilder) CmdBuilder.getBuilder(this, command, plugin.getName()));
		
		return baseBuilders.getLast();
	}
	
	protected void addCommand(Command c){
		bases.add(c);
		
		plugin.getCommand(c.getName()).setTabCompleter(tabCompleter);
	}
}
