/**
This file is part of Commandler.

Commandler is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Commandler is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Commandler.  If not, see <http://www.gnu.org/licenses/>.
*/

package com.jam.commandler.Exception;

public class NumberArgumentNotWithinRangeException extends CommandArgumentException{
	private static final long serialVersionUID = 4481671616102552672L;
	
	public int min, max;
	
	public NumberArgumentNotWithinRangeException(int min, int max){
		this.min = min;
		this.max = max;
	}
}
