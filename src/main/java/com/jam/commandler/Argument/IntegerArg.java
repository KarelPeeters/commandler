/**
This file is part of Commandler.

Commandler is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Commandler is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Commandler.  If not, see <http://www.gnu.org/licenses/>.
*/

package com.jam.commandler.Argument;

import com.jam.commandler.Exception.NotANumberArgumentException;
import com.jam.commandler.Exception.NumberArgumentNotWithinRangeException;

public class IntegerArg extends CountableArg<Integer>{
	private Integer min, max;
	
	public IntegerArg(String name) {
		this(name, false);
	}
	
	public IntegerArg(String name, boolean optional) {
		super(name, optional);
	}
	
	public IntegerArg(String name, boolean optional, int minInclusive, int maxExclusive){
		super(name, optional);
		
		this.min = minInclusive;
		this.max = maxExclusive;
	}
	
	public IntegerArg(String name, int minInclusive, int maxExclusive){
		this(name, false, minInclusive, maxExclusive);
	}

	@Override
	public Integer process(CommandSession session) {
		try {
			int number = Integer.parseInt(session.getRawArgIterator().next());
			
			if (min != null){
				if (number < min || number >= max){
					throw new NumberArgumentNotWithinRangeException(min, max);
				}
			}

			return number;
		} catch (NumberFormatException e){
			throw new NotANumberArgumentException();
		}
	}

	@Override
	public int getConsumedCount() {
		return 1;
	}
}
