/**
This file is part of Commandler.

Commandler is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Commandler is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Commandler.  If not, see <http://www.gnu.org/licenses/>.
*/

package com.jam.commandler.Argument;

public class MergeRemainingArg extends Arg<String>{

	public MergeRemainingArg(String name) {
		this(name, false);
	}
	
	public MergeRemainingArg(String name, boolean optional) {
		super(name, optional);
	}

	@Override
	public String process(CommandSession session) {
		RawArgIterator iter = session.getRawArgIterator();
		StringBuilder builder = new StringBuilder(iter.next());
		
		while (iter.hasNext()){
			builder.append(" ");
			builder.append(iter.next());
		}
		
		return builder.toString();
	}

}
