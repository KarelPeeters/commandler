/**
This file is part of Commandler.

Commandler is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Commandler is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Commandler.  If not, see <http://www.gnu.org/licenses/>.
*/

package com.jam.commandler.Argument;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.BiConsumer;

@SuppressWarnings("rawtypes")
public class ProcessedArguments {
	private Map<String, Arg> args;
	private Map<String, Object> proc;
	
	public ProcessedArguments(Iterable<? extends Arg> args) {
		this.proc = new LinkedHashMap<>();
		this.args = new LinkedHashMap<>();
		
		args.forEach(a -> this.args.put(a.getName(), a));
	}
	
	public void processArgs(CommandSession session){
		args.forEach((key, arg) -> proc.put(arg.getName(), arg.extractArgument(session)));

		insertExcessArgs(session);
	}
	
	private void insertExcessArgs(CommandSession session) {
		int nextIndex = 0;
		while (session.getRawArgIterator().hasNext()) {
			ExcessArg ex = new ExcessArg(nextIndex++);

			proc.put(ex.getName(), ex.extractArgument(session));
		}
	}

	public Object getProcessed(String key){
		return proc.getOrDefault(key, null);
	}
	
	public int size(){
		return proc.size();
	}
	
	public void forEach(BiConsumer<? super String, ? super Object> consumer){
		proc.forEach(consumer);
	}
	
	public Collection<Arg> argList(){
		return args.values();
	}
}
