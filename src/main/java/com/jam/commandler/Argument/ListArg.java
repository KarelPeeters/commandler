/**
This file is part of Commandler.

Commandler is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Commandler is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Commandler.  If not, see <http://www.gnu.org/licenses/>.
*/

package com.jam.commandler.Argument;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public abstract class ListArg<T> extends CountableArg<List<T>> implements Function<String, T>{
	protected String separator = ",";

	public ListArg(String name) {
		super(name);
	}
	
	public ListArg(String name, String separator){
		super(name);
		
		this.separator = separator;
	}
	
	public ListArg(String name, String separator, boolean optional){
		super(name, optional);
		
		this.separator = separator;
	}
	
	public ListArg(String name, boolean optional){
		super(name, optional);
	}

	@Override
	public List<T> process(CommandSession session) {
		String raw = session.getRawArgIterator().next();
		
		List<String> rawSplit = new ArrayList<>();
		rawSplit.addAll(Arrays.asList(raw.split(separator)));
		
		return rawSplit.stream()
				.map(this)
				.collect(Collectors.toList());
	}
	
	@Override
	public int getConsumedCount() {
		return 1;
	}
}
