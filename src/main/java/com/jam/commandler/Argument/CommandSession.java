/**
This file is part of Commandler.

Commandler is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Commandler is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Commandler.  If not, see <http://www.gnu.org/licenses/>.
*/

package com.jam.commandler.Argument;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.command.CommandSender;

@SuppressWarnings("rawtypes")
public class CommandSession {
	private CommandSender sender;
	private RawArgIterator rawIterator; //iterator soon
	private List<String> base;
	private ProcessedArguments processed;

	public CommandSession(CommandSender sender, String commandName, String[] rawArgs){
		this.sender = sender;
		this.rawIterator = new RawArgIterator(rawArgs);
		this.base = new ArrayList<>();
		
		this.base.add(commandName);
	}
	
	public String getBase(){
		return base.stream().collect(Collectors.joining(" "));
	}
	
	public CommandSender getSender(){
		return sender;
	}
	
	public void appendCurrentToBase(){
		base.add(rawIterator.peek());
	}
	
	public void processArgs(Iterable<? extends Arg> args){
		processed = new ProcessedArguments(args);
		processed.processArgs(this);
	}
	
	public ProcessedArguments getProcessedArgs(){
		return processed;
	}
	
	public Object getProcessed(String key){
		return processed.getProcessed(key);
	}
	
	public RawArgIterator getRawArgIterator(){
		return rawIterator;
	}
}
