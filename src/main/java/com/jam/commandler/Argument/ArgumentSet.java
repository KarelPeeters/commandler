package com.jam.commandler.Argument;

import java.util.LinkedHashSet;

@SuppressWarnings("rawtypes")
public class ArgumentSet extends LinkedHashSet<CountableArg> {
	private static final long serialVersionUID = 1L;

	public ArgumentSet(CountableArg... args) {
		for (CountableArg a : args){
			add(a);
		}
	}
	
	public int getConsumedCount(){
		int sum = 0;

		for (CountableArg a : this){
			sum += a.getConsumedCount();
		}
		
		return sum;
	}
	
	@Override
	public int hashCode() {
		return getConsumedCount();
	}
	
	@Override
	public boolean equals(Object arg) {
		if (arg instanceof ArgumentSet){
			ArgumentSet a = (ArgumentSet) arg;
			
			return this.equals(a);
		}
		
		return false;
	}
}
