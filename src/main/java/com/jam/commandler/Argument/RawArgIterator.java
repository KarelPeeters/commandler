/**
This file is part of Commandler.

Commandler is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Commandler is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Commandler.  If not, see <http://www.gnu.org/licenses/>.
*/

package com.jam.commandler.Argument;

import java.util.Iterator;

public class RawArgIterator implements Iterator<String>{
	private String[] rawArgs;
	private int current;
	
	public RawArgIterator(String[] rawArgs) {
		this.rawArgs = rawArgs;
		this.current = 0;
	}

	@Override
	public boolean hasNext() {
		return current < rawArgs.length;
	}

	@Override
	public String next() {
		return rawArgs[current++];
	}
	
	public String peek() {
		if (current == 0) 
			throw new IllegalAccessError("No current. Use getNextRaw at least once before using this");
		
		return rawArgs[current - 1];
	}
	
	public String last(){
		String next = null;
		
		while (hasNext()){
			next = next();
		}
		
		return next;
	}
	
	public int getRemainingCount(){
		return rawArgs.length - current;
	}
}
