/**
This file is part of Commandler.

Commandler is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Commandler is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Commandler.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.jam.commandler.Argument;

import com.jam.commandler.Exception.CommandArgumentException;
import com.jam.commandler.Exception.MissingArgumentException;

public abstract class Arg<T> {
	protected static final String[] DEFAULT_ARG_DELIMITERS = new String[] {"<", ">"};
	protected static final String[] DEFAULT_OPT_ARG_DELIMITERS = new String[] {"[", "]"};
	
	protected String name;
	protected boolean optional;

	public Arg(String name, boolean optional) {
		this.name = name;
		this.optional = optional;
	}

	public Arg(String name) {
		this(name, false);
	}

	public String getName(){
		return name;
	}
	
	public String getSignature(){
		if (optional){
			return DEFAULT_OPT_ARG_DELIMITERS[0] + name + DEFAULT_OPT_ARG_DELIMITERS[1];
		} else {
			return DEFAULT_ARG_DELIMITERS[0] + name + DEFAULT_ARG_DELIMITERS[1];
		}
	}
	
	public T extractArgument(CommandSession session) throws CommandArgumentException{
		if (!session.getRawArgIterator().hasNext()){
			if (optional){
				return null;
			} else {
				throw new MissingArgumentException();				
			}
		}
		
		return process(session);
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Arg){
			Arg a = (Arg) obj;
			
			return a.getName() == getName();
		}
		
		return false;
	}
	
	public abstract T process(CommandSession session);
}
