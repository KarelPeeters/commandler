/**
This file is part of Commandler.

Commandler is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Commandler is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Commandler.  If not, see <http://www.gnu.org/licenses/>.
*/

package com.jam.commandler.Argument;

import com.jam.commandler.Exception.MissingArgumentDelimiterException;

public class MultiStringArg extends Arg<String>{
	private static final String DELIMITER = "\"";
	
	public MultiStringArg(String name) {
		this(name, false);
	}
	
	public MultiStringArg(String name, boolean optional) {
		super(name, optional);
	}

	@Override
	public String process(CommandSession session) {
		RawArgIterator iter = session.getRawArgIterator();
		
		StringBuilder result = new StringBuilder(iter.next());
		
		if (result.indexOf(DELIMITER) == -1){
			return result.toString();
		} else {
			result.deleteCharAt(result.indexOf(DELIMITER));
			
			while (result.indexOf(DELIMITER) == -1){
				if (!iter.hasNext()){
					throw new MissingArgumentDelimiterException();
				}
				
				result.append(' ');
				result.append(iter.next());
			}

			result.deleteCharAt(result.indexOf(DELIMITER));
			
			return result.toString();
		}
	}
}
