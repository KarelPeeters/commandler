# Commandler
Commandler is an API to make creating commands in Bukkit/Spigot plugins easier. It was originally meant to allow for easy nested command setup, but simple commands are supported as well. It allows for easy command aliases, autocompletion, has built-in colored error handling, and makes argument parsing much more organised.

### Why should I use it?
Why should you use an API when its pretty easy to write commands without it? There are a few reasons:

* Easy and concise command definition- both normal and nested commands
* No more repetitive if-statements to check the argument count
* Built-in permissions, no checks and setup required
* Informative colored error messages
* Support for custom argument parsers
* Overloaded arguments- different argument set for the same command
* Support for custom errors and error handlers
* QA: Almost 100% test coverage

Granted, defining and implementing commands without this is easy, but the implementation gets messy and repetitive, especially when multiple arguments are involved. Code snippets below demonstrate how easy it is to implement commands with this library.

### Planned features
These are the features that I plan to add to the library:

* Per-argument tab-completion
* Overloaded commands- different argument and behavior for the same command
* CommandSender whitelist

### Setup
To use Commandler in your plugin, add the following repository and dependency to your `pom.xml`:
```xml
<repository>
	<id>spinalcraft-repo</id>
	<url>http://maven.spinalcraft.com</url>
</repository>
```

```xml
<dependency>
	<groupId>com.jam.commandler</groupId>
	<artifactId>Commandler</artifactId>
	<version>1.0.0</version>
</dependency>
```
### Passing control to Commandler
The basis for the Commandler API is the `Commandler` class:
```java
@Override
public void onEnable() {
    commandler = new Commandler(this);

    getCommand("kill").setExecutor(commandler);
    getCommand("sp").setExecutor(commandler);
    getCommand("red").setExecutor(commandler);
}
```
It implements the Bukkit `CommandExecutor` interface. You have to set it as the executor for your base-commands (the commands declared in your `plugin.yml`). `Commandler` accepts `JavaPlugin` as a constructor argument in order to handle permissions correctly.

### Where to put the commands
If you so choose, you can define the commands directly in your `onEnable()` method by getting the `CommandFactory` for the desired command:
```java
TypeBuilderStep f = commandler.getCommandBuilder("sp");
```
If you want to separate the command definitions from your plugin's setup code, use the `CommandlerBuilder` interface:
```java
public class CommandBuilder implements CommandlerBuilder {
    @Override
	public void build(Commandler commandler) {
	    //TODO: Command definitions here
    }
}
```
```java
commandler.fromBuilder(new CommandBuilder());
```

### Defining simple commands
To define commands, use the factories obtained from the `Commandler` instance like so:
```java
commandler.getCommandBuilder("red")
	.setBehavior(session -> cmdRed(session))
	.addArg(new StringArg("word"));

commandler.getCommandBuilder("kill")
	.setBehavior(session -> cmdKill(session))
	.addArg(new PlayerArg("player"))
	.setArgErrorHandler(killErrorHandler);
	
commandler.getCommandBuilder("sp")
	.setBehavior(session -> cmdSayPlayer(session))
	.addArg(new PlayerArg("player"))
	.addArg(new MergeRestArg("message"));
```
Of course the `Behavior` can also be implemented as a normal or anonymous inner class. The chain call automatically disallows most invalid combinations of the command setup.

### Defining behavior
The `setBehavior` method requires an instance of `Behavior` interface, for example:
```java
public class CommandRedBehavior implements Behavior {
    @Override
    void execute(CommandSession s){
        String word = (String) s.getProcessed("word");
        
        s.getSender().sendMessage(CharColor.RED + word);
    }
}
```
As you can see, arguments from the user are passed to the `execute` method to be used in the command. The key for the argument is the String provided during definition of the command argument.

### Custom Args
Classes that can be passed to the `addArg` method have to extend `Arg` or one of its subclasses. The following `Arg` subclass will read one *String* from the arguments and check if its a valid player:
```java
private class PlayerArg extends Arg<Player> {
	public PlayerArg(String name) {
		super(name);
	}

	@Override
	public Player process(CommandSession session) {
		String stringArg = session.getRawArgIterator.next();
		Player target = Bukkit.getServer().getPlayer(stringArg);
		if (target == null){
			throw new PlayerOfflineException(stringArg);
		}

		return target;			
	}
}
```
Commandler provides some of built-in `Args` such as `StringArg` that takes one *String* from the input and `MergeRemainingArg` that takes all remaining arguments and returns them as one *String*, `IntegerArg` that returns an integer, and `MultiStringArg` that returns Strings surrounded by quotes, preserving the spaces and finally `StringListArg` that takes a list of values, by default separated by a comma.

### Error handling
You can throw subclasses of `CommandArgumentException` from `Arg.process(...)` commands when the arguments the user passed are incorrect. To handle these exceptions, you need to extend `ArgumentErrorHandler` or one of its subclasses:
```java
private ArgumentErrorHandler playerArgumentHandler = new DefaultArgumentErrorHandler() {
	@Override
	public void handle(CommandSession session, CommandArgumentException error) {
		if (error instanceof PlayerOfflineException){
			PlayerOfflineException e = (PlayerOfflineException) error;
			
			sender.sendMessage(ChatColor.DARK_AQUA + session.getRawArgIterator().peek() + ChatColor.RED + " is not online!");
		} else {
		    super.handle(session, error);
		}
	}
};
```

##### Default error handler
If no *error handler* is specified when defining a command, the default handler will be used. It handles many exceptions thrown by built-in `Args`

The picture below shows the error messages printed for missing arguments:

![alt text](https://bytebucket.org/JamMaster/commandler/raw/1651d8f4bb014f3388d8112577e133a9d8bebe9f/commandlererror.png?token=5822bf040be568de8773b7837dd0ef696d734fe5)

On the picture the *sp* command was first invoked without any arguments and then with a correct player name.

### Nested commands
To create a nested command hierarchy, you can use the `addSubcommand` method that will return a separate factory for that sub command.
```java
TypeBuilderStep fac = cmdlr.getCommandBuilder("filter");

TypeBuilderStep facWord = fac.addSubcommand("word");
TypeBuilderStep facGroup = fac.addSubcommand("group");

TypeBuilderStep facWordAdd = facWord.addSubcommand("add");
TypeBuilderStep facGroupAdd = facGroup.addSubcommand("add");
```
The leaf-commands (commands at the ends of the command-tree) can be assigned `Behaviors`, `Arg` and the rest just like a normal command. When the command hierarchy is ready, call `Commandler.build` on and all the defined commands will be built:
```java
commandler.build();
```
### Permissions
Permissions are seamlessly integrated into `Commandler`. You don't need to do any work. The permission pattern is the plugin name followed by a dot and command (or command chain) name:
```
PluginName.command.subcommand1.subcommand2
```
When user enters a wrong command and the error handler gives the user a hint on how to properly run the commands, it automatically omits commands that the player does not have the permissions to use:

After issuing */fltr* when only having permission to `chatfilter.fltr.word.add`

![alt text](https://bytebucket.org/JamMaster/commandler/raw/fc79daa81b5f403c162ed7808363c36ccf6e8700/commandlerpermnolist1.png)

Adter issuing */fltr* when having permissions to both `chatfilter.fltr.word.add` and `chatfilter.fltr.group.add`

![alt text](https://bytebucket.org/JamMaster/commandler/raw/fc79daa81b5f403c162ed7808363c36ccf6e8700/commandlerpermnolist2.png)

### TabCompleters
Commandler provides built-in tabCompletion for sub-commands. Simply press tab after having typed a part of a command to autocomplete it. Commandler will not autocomplete commands that the caller does not have the permission to use. Per-argument custom tabCompleters are a planned feature.

### Overloaded Argument Sets
In order to provide multiple sets of possible arguments, like in the vanilla minecraft command `tp`:

* `tp <x> <y> <z>`
* `tp <player>`
* `tp <whichPlayer> <toPlayer>`

you need to add `Args` grouped in `ArgumentSets` using the `addArgumentSet`. Using this method disallows from adding `Args` that are not a part of a set for the current command. Also, in order for the API to be able to tell which set to choose, `Args` in the set have to extend `CountableArg`, that also requires you to specify how many raw arguments your `Arg` is going to consume. Here is an example usage:

```java
cmdlr.getCommandBuilder("tp")
				.setBehavior(session -> {
					//TODO: Implement behavior
				})
				.addArgumentSet(new ArgumentSet(
						new CoordArg("x"),
						new CoordArg("y"),
						new CoordArg("z")))
				.addArgumentSet(new ArgumentSet(
						new PlayerArg("player")))
				.addArgumentSet(new ArgumentSet(
				        new PlayerArg("whichPlayer"),
				        new PlayerArg("toPlayer")));
```

Keep in mind that for this to work, the `ArgumentSets` may not consume the same amount raw arguments. If they do, an exception will be thrown during the building process.